package steps;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Keyboard;
import utils.DriversUtils;

import java.util.List;

public class GoogleStepdefs {

    @Given("^I go to \"([^\"]*)\"$")
    public void iGoTo(String arg0) throws Throwable {
        DriversUtils.driver.get(arg0);
    }

    @When("I click on Le Quesnoy market")
    public void i_click_on_le_quesnoy_market() {
        DriversUtils.fluentWait(By.xpath("//a[contains(@title, \"Le Quesnoy\")]")).click();
    }

    @Then("The Log In button is displayed")
    public void the_log_in_button_is_displayed() {
        Assert.assertTrue(DriversUtils.fluentWait(By.id("pop")).isDisplayed());
      }
}
