package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.function.Function;

public class DriversUtils {
    public static WebDriver driver = createDriver();

    //TODO lors de la MEP changer les drivers
    private static WebDriver createDriver() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        return new ChromeDriver(capabilities);
    }

    public static WebElement fluentWait(By selector){
        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(Exception.class)
                .withMessage("Time expired");

        return wait.until(new Function<WebDriver,WebElement>(){
            public WebElement apply(WebDriver arg0) {
                return driver.findElement(selector);}
        });
    }
}
